#pragma once

/// @defgroup Network Network
/// @ingroup Core
namespace derelict
{
struct Socket;
struct IPAddress;

} // namespace derelict
