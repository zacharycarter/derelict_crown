#pragma once

#include "functional.h"
#include "math_types.h"
#include "string_id.h"
#include "types.h"

/// @defgroup World World
namespace derelict
{
const u32 RESOURCE_PACKAGE_MARKER = 0x9a1ac68c;
} // namespace derelict
