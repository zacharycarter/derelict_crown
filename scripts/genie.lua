DERELICT_DIR = (path.getabsolute("..") .. "/")
local DERELICT_THIRD_DIR = (DERELICT_DIR .. "3rdparty/")
local DERELICT_BUILD_DIR = (DERELICT_DIR .. "build/")
BGFX_DIR = (DERELICT_DIR .. "3rdparty/bgfx/")
BX_DIR = (DERELICT_DIR .. "3rdparty/bx/")
local BGFX_BUILD_DIR = path.join("../", "build")
local BGFX_THIRD_PARTY_DIR = path.join(BGFX_DIR, "3rdparty")

local BGFX_BUILD_DIR = path.join("../", "build")
local BGFX_THIRD_PARTY_DIR = path.join(BGFX_DIR, "3rdparty")

function copyLib()
end
newoption {
	trigger = "with-luajit",
	description = "Build with luajit support."
}
solution "derelict"
	configurations {
		"debug",
		"release",
	}
	platforms {
		"x32",
		"x64",
		"native"
	}
	language "C++"
	configuration {}

defines {
	"BX_CONFIG_ENABLE_MSVC_LEVEL4_WARNINGS=1"
}

dofile (path.join(BX_DIR, "scripts/toolchain.lua"))
if not toolchain(BGFX_BUILD_DIR, BGFX_THIRD_PARTY_DIR) then
	return -- no action specified
end

dofile (BGFX_DIR .. "scripts/bgfx.lua")


dofile (BGFX_DIR .. "scripts/bgfx.lua")

group "libs"
bgfxProject("", "StaticLib", os.is("windows") and { "BGFX_CONFIG_RENDERER_DIRECT3D9=1" } or {})

dofile(path.join(BX_DIR, "scripts/bx.lua"))

dofile ("derelict.lua")

group "derelict"
derelict_project("", "ConsoleApp", {})

configuration { "osx" }
	files {
		DERELICT_DIR .. "src/**.mm"
	}
   	linkoptions {
		"-framework Cocoa",
		"-framework QuartzCore",
		"-framework OpenGL",
		"-weak_framework Metal",
	}
configuration { "osx" }
	postbuildcommands {
		"cp    " .. DERELICT_THIRD_DIR .. "LuaJIT/pre/osx_x64/luajit " .. DERELICT_BUILD_DIR .. "osx64_clang/bin",
		"cp -r " .. DERELICT_THIRD_DIR .. "LuaJIT/src/jit "              .. DERELICT_BUILD_DIR .. "osx64_clang/bin",
	}
configuration {}
strip()
