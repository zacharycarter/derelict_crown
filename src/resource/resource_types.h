#pragma once

/// @defgroup Resource Resource
namespace derelict
{
class ResourceLoader;
class ResourceManager;
struct ResourcePackage;

struct LuaResource;

} // namespace derelict

/// @addtogroup Resource
/// @{
#define RESOURCE_TYPE_CONFIG           StringId64(0x82645835e6b73232)
#define RESOURCE_TYPE_PACKAGE          StringId64(0xad9c6d9ed1e5e77a)
#define RESOURCE_TYPE_SCRIPT           StringId64(0xa14e8dfa2cd117e2)

#define RESOURCE_VERSION_CONFIG           u32(1)
#define RESOURCE_VERSION_PACKAGE          u32(1)
#define RESOURCE_VERSION_SCRIPT           u32(1)
/// @}
