#pragma once

#include "platform.h"

namespace derelict
{
/// Atomic integer.
///
/// @ingroup Thread
struct AtomicInt
{
#if DERELICT_PLATFORM_POSIX && (DERELICT_COMPILER_GCC || DERELICT_COMPILER_CLANG)
    mutable int _val;
#endif
    AtomicInt(int val)
	{
		store(val);
	}

	int load() const
	{
#if DERELICT_PLATFORM_POSIX && (DERELICT_COMPILER_GCC || DERELICT_COMPILER_CLANG)
        __sync_fetch_and_add(&_val, 0);
		return _val;
#endif
    }

    void store(int val)
    {
#if DERELICT_PLATFORM_POSIX && (DERELICT_COMPILER_GCC || DERELICT_COMPILER_CLANG)
        __sync_lock_test_and_set(&_val, val);
#endif
    }
};

} // namespace derelict
