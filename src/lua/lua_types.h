#pragma once

/// @defgroup Lua Lua
namespace derelict
{
struct LuaStack;
struct LuaEnvironment;

} // namespace derelict
