#include "config.h"

#if DERELICT_PLATFORM_OSX

#include "array.h"
#include "command_line.h"
#include "device.h"
#include "device_event_queue.h"
#include "os.h"
#include "thread.h"
#include "window.h"
#include <iostream>
#include <string>
#include <bgfx/platform.h>
#include <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject<NSApplicationDelegate>
{
	bool terminated;
}

+ (AppDelegate *)sharedDelegate;
- (id)init;
- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender;
- (bool)applicationHasTerminated;

@end

@interface Window : NSObject<NSWindowDelegate>
{
	uint32_t windowCount;
}

+ (Window*)sharedDelegate;
- (id)init;
- (void)windowCreated:(NSWindow*)window;
- (void)windowWillClose:(NSNotification*)notification;
- (BOOL)windowShouldClose:(NSWindow*)window;
- (void)windowDidResize:(NSNotification*)notification;
- (void)windowDidBecomeKey:(NSNotification *)notification;
- (void)windowDidResignKey:(NSNotification *)notification;

@end


namespace derelict
{
static KeyboardButton::Enum cocoa_translate_key(NSString* key)
{
    if([key length] == 0)
    {
        return KeyboardButton::COUNT;
    }

    int keyCode = [key characterAtIndex:0];
    switch(keyCode)
    {
    case 27:    return KeyboardButton::ESCAPE;
    default:    return KeyboardButton::COUNT;
    }


}

static bool s_exit = false;

struct MainThreadArgs
{
    DeviceOptions* opts;
};

s32 func(void* data)
{
	MainThreadArgs* args = (MainThreadArgs*)data;
	derelict::run(*args->opts);
	s_exit = true;
	return EXIT_SUCCESS;
}

struct OSXDevice
{
    OSXDevice()
        : _style(0)
    {
    }

    NSEvent* peekEvent()
    {
        return [NSApp
            nextEventMatchingMask:NSAnyEventMask
            untilDate:[NSDate distantPast] // do not wait for event
            inMode:NSDefaultRunLoopMode
            dequeue:YES
            ];
    }

    bool dispatchEvent(NSEvent* event)
    {
        if(event)
        {
            NSEventType eventType = [event type];

            switch (eventType)
            {
                case NSKeyUp:
                case NSKeyDown:
                {
                    KeyboardButton::Enum key = cocoa_translate_key([event charactersIgnoringModifiers]);

                    if (key != KeyboardButton::COUNT)
                    {
                        _queue.push_button_event(InputDeviceType::KEYBOARD
                            , 0
                            , key
                            , eventType == NSKeyDown
                            );
                    }

                }
                break;
            }

            [NSApp sendEvent:event];
            [NSApp updateWindows];

            return true;
        }
        return false;
    }

    int run(DeviceOptions* opts)
    {
        [NSApplication sharedApplication];

        id dg = [AppDelegate sharedDelegate];
		[NSApp setDelegate:dg];
		[NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
		[NSApp activateIgnoringOtherApps:YES];
		[NSApp finishLaunching];

		[[NSNotificationCenter defaultCenter]
			postNotificationName:NSApplicationWillFinishLaunchingNotification
			object:NSApp];

		[[NSNotificationCenter defaultCenter]
			postNotificationName:NSApplicationDidFinishLaunchingNotification
			object:NSApp];

		id quitMenuItem = [NSMenuItem new];
		[quitMenuItem
			initWithTitle:@"Quit"
			action:@selector(terminate:)
			keyEquivalent:@"q"];

		id appMenu = [NSMenu new];
		[appMenu addItem:quitMenuItem];

		id appMenuItem = [NSMenuItem new];
		[appMenuItem setSubmenu:appMenu];

		id menubar = [[NSMenu new] autorelease];
		[menubar addItem:appMenuItem];
		[NSApp setMainMenu:menubar];

        NSRect screenRect = [[NSScreen mainScreen] frame];
		const float centerX = (screenRect.size.width  - (float)opts->_window_width )*0.5f;
		const float centerY = (screenRect.size.height - (float)opts->_window_height)*0.5f;

        _style = 0
                | NSTitledWindowMask
                | NSClosableWindowMask
                | NSMiniaturizableWindowMask
                | NSResizableWindowMask
                ;

        NSRect rect = NSMakeRect(centerX, centerY, opts->_window_width, opts->_window_height);
        _window_handle = [[NSWindow alloc]
			initWithContentRect:rect
			styleMask:_style
			backing:NSBackingStoreBuffered defer:NO
		];
        NSString* appName = [[NSProcessInfo processInfo] processName];
		[_window_handle setTitle:appName];
		[_window_handle makeKeyAndOrderFront:_window_handle];
		[_window_handle setAcceptsMouseMovedEvents:YES];
		[_window_handle setBackgroundColor:[NSColor blackColor]];
		[[Window sharedDelegate] windowCreated:_window_handle];

        // Start main thread
        MainThreadArgs mta;
        mta.opts = opts;

        Thread main_thread;
        main_thread.start(func, &mta);

        while (!(s_exit= [dg applicationHasTerminated]) )
        {
            @autoreleasepool
            {
                if (bgfx::RenderFrame::Exiting == bgfx::renderFrame() )
                {
                    break;
                }
            }

            while(dispatchEvent(peekEvent() ) )
            {

            }
        }

        _queue.push_exit_event();

        while (bgfx::RenderFrame::NoContext != bgfx::renderFrame() ) {};
        main_thread.stop();

        return EXIT_SUCCESS;
    }

public:
    NSWindow* _window_handle; 
    DeviceEventQueue _queue;
    int32_t _style;
};

static OSXDevice s_odvc;

struct DeviceWindowOSX : public DeviceWindow
{
    DeviceWindowOSX()
    {

    }

    void open(u16 x, u16 y, u16 width, u16 height, u32 /*parent*/)
	{
	}

    void close()
    {
        dispatch_async(dispatch_get_main_queue()
		, ^{
		  [s_odvc._window_handle performClose: nil];
		}); 
    }

    void bgfx_setup()
    {
        bgfx::PlatformData pd;
        pd.ndt          = NULL;
        pd.nwh          = s_odvc._window_handle;
        pd.context      = NULL;
        pd.backBuffer   = NULL;
        pd.backBufferDS = NULL;
        bgfx::setPlatformData(pd);
    }

    void show()
    {

    }

    void hide()
    {

    }

    void resize(u16 width, u16 height)
    {

    }

    void move(u16 x, u16 y)
    {

    }

    void minimize()
    {

    }
    
    void restore()
    {

    }

    const char* title()
    {
        return "";
    }

    void set_title(const char* title)
    {

    }
    
    void show_cursor(bool show)
    {

    }

    void set_fullscreen(bool /*fullscreen*/)
	{

	}

    void* handle()
    {
        return (void*)_window_handle;
    }

    NSWindow* _window_handle;
};

namespace window
{
    DeviceWindow* create(Allocator& a)
    {
        return DE_NEW(a, DeviceWindowOSX)();
    }

    void destroy(Allocator& a, DeviceWindow& w)
    {
        DE_DELETE(a, &w);
    }
} // namespace window

struct DisplayOSX : public Display
{
    void modes(Array<DisplayMode>& /*modes*/)
    {   
    }

    void set_mode(u32 /*id*/)
    {
    }
};

namespace display
{
    Display* create(Allocator &a)
    {
        return DE_NEW(a, DisplayOSX)();
    }

    void destroy(Allocator& a, Display& d)
    {
        DE_DELETE(a, &d);
    }
} // namespace display

bool next_event(OsEvent& ev)
{
    return s_odvc._queue.pop_event(ev);
}

} // namespace derelict

@implementation AppDelegate

+ (AppDelegate *)sharedDelegate
{
	static id delegate = [AppDelegate new];
	return delegate;
}

- (id)init
{
	self = [super init];

	if (nil == self)
	{
		return nil;
	}

	self->terminated = false;
	return self;
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
	DE_UNUSED(sender);
	self->terminated = true;
	return NSTerminateCancel;
}

- (bool)applicationHasTerminated
{
	return self->terminated;
}

@end

@implementation Window

+ (Window*)sharedDelegate
{
	static id windowDelegate = [Window new];
	return windowDelegate;
}

- (id)init
{
	self = [super init];
	if (nil == self)
	{
		return nil;
	}

	self->windowCount = 0;
	return self;
}

- (void)windowCreated:(NSWindow*)window
{
	assert(window);

	[window setDelegate:self];

	assert(self->windowCount < ~0u);
	self->windowCount += 1;
}

- (void)windowWillClose:(NSNotification*)notification
{
	DE_UNUSED(notification);
}

- (BOOL)windowShouldClose:(NSWindow*)window
{
	assert(window);

	[window setDelegate:nil];

	assert(self->windowCount);
	self->windowCount -= 1;

	if (self->windowCount == 0)
	{
		[NSApp terminate:self];
		return false;
	}

	return true;
}

- (void)windowDidResize:(NSNotification*)notification
{
	DE_UNUSED(notification);
	//using namespace entry;
	//s_ctx.windowDidResize();
}

- (void)windowDidBecomeKey:(NSNotification*)notification
{
    DE_UNUSED(notification);
    //using namespace entry;
    //s_ctx.windowDidBecomeKey();
}

- (void)windowDidResignKey:(NSNotification*)notification
{
    DE_UNUSED(notification);
    //using namespace entry;
    //s_ctx.windowDidResignKey();
}

@end

struct InitMemoryGlobals
{
	InitMemoryGlobals()
	{
		derelict::memory_globals::init();
	}

	~InitMemoryGlobals()
	{
		derelict::memory_globals::shutdown();
	}
};

int main(int argc, char** argv)
{
    using namespace derelict;
    InitMemoryGlobals m;
    DE_UNUSED(m);

    DeviceOptions opts(argc, (const char**)argv);
	if (opts.parse() != EXIT_SUCCESS)
		return EXIT_FAILURE;

	return s_odvc.run(&opts);
}


#endif // DERELICT_PLATFORM_OSX
