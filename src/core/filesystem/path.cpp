#include "dynamic_string.h"
#include "path.h"
#include <ctype.h> // isalpha
#include <string.h> // strrchr

namespace derelict
{
namespace path
{
	bool is_absolute(const char* path)
	{
		DE_ENSURE(NULL != path);
#if DERELICT_PLATFORM_POSIX
		return strlen32(path) > 0
			&& path[0] == PATH_SEPARATOR
			;
#endif
	}

	bool is_relative(const char* path)
	{
		DE_ENSURE(NULL != path);
		return !is_absolute(path);
	}

	bool is_root(const char* path)
	{
		DE_ENSURE(NULL != path);
#if DERELICT_PLATFORM_POSIX
		return is_absolute(path) && strlen32(path) == 1;
#endif
	}

	void join(const char* path_a, const char* path_b, DynamicString& path)
	{
		DE_ENSURE(NULL != path_a);
		DE_ENSURE(NULL != path_b);
		const u32 la = strlen32(path_a);
		const u32 lb = strlen32(path_b);
		path.reserve(la + lb + 1);
		path += path_a;
		path += PATH_SEPARATOR;
		path += path_b;
	}

	const char* basename(const char* path)
	{
		DE_ENSURE(NULL != path);
		const char* ls = strrchr(path, '/');
		return ls == NULL ? path : ls + 1;
	}

	const char* extension(const char* path)
	{
		DE_ENSURE(NULL != path);
		const char* ld = strrchr(path, '.');
		return ld == NULL ? NULL : ld + 1;
	}
} // namespace path

} // namespace derelict
