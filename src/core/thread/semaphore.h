#pragma once

#include "error.h"
#include "mutex.h"
#include "platform.h"

#if DERELICT_PLATFORM_POSIX
    #include <pthread.h>
#endif

namespace derelict
{
/// Semaphore.
///
/// @ingroup Thread.
struct Semaphore
{
#if DERELICT_PLATFORM_POSIX
	Mutex _mutex;
	pthread_cond_t _cond;
	s32 _count;
#endif

	Semaphore()
#if DERELICT_PLATFORM_POSIX
		: _count(0)
#endif
	{
#if DERELICT_PLATFORM_POSIX
		int err = pthread_cond_init(&_cond, NULL);
		DE_ASSERT(err == 0, "pthread_cond_init: errno = %d", err);
		DE_UNUSED(err);
#endif
	}

	~Semaphore()
	{
#if DERELICT_PLATFORM_POSIX
		int err = pthread_cond_destroy(&_cond);
		DE_ASSERT(err == 0, "pthread_cond_destroy: errno = %d", err);
		DE_UNUSED(err);
#endif
	}

	void post(u32 count = 1)
	{
#if DERELICT_PLATFORM_POSIX
		ScopedMutex sm(_mutex);

		for (u32 i = 0; i < count; ++i)
		{
			int err = pthread_cond_signal(&_cond);
			DE_ASSERT(err == 0, "pthread_cond_signal: errno = %d", err);
			DE_UNUSED(err);
		}

		_count += count;
#endif
	}

	void wait()
	{
#if DERELICT_PLATFORM_POSIX
		ScopedMutex sm(_mutex);

		while (_count <= 0)
		{
			int err = pthread_cond_wait(&_cond, &(_mutex._mutex));
			DE_ASSERT(err == 0, "pthread_cond_wait: errno = %d", err);
			DE_UNUSED(err);
		}

		_count--;
#endif
	}

private:

	// Disable copying
	Semaphore(const Semaphore& s);
	Semaphore& operator=(const Semaphore& s);
};

} // namespace derelict
