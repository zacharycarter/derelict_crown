#pragma once

#include "dynamic_string.h"
#include "string_id.h"
#include "types.h"

namespace derelict
{
/// Boot configuration.
///
/// @ingroup Device
struct BootConfig
{
	StringId64 boot_script_name;
	StringId64 boot_package_name;
	DynamicString window_title;
	u16 window_w;
	u16 window_h;
	float aspect_ratio;
	bool vsync;
	bool fullscreen;

	BootConfig(Allocator& a);
	bool parse(const char* json);
};

} // namespace derelict
