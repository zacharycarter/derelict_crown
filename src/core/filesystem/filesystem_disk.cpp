#include "dynamic_string.h"
#include "file.h"
#include "filesystem_disk.h"
#include "os.h"
#include "path.h"
#include "temp_allocator.h"
#include "vector.h"

#if DERELICT_PLATFORM_POSIX
    #include <stdio.h>
    #include <errno.h>
#endif

namespace derelict
{
class FileDisk : public File
{
#if DERELICT_PLATFORM_POSIX
	FILE* _file;
#endif

public:

	/// Opens the file located at @a path with the given @a mode.
	FileDisk()
#if DERELICT_PLATFORM_POSIX
		: _file(NULL)
#endif
	{
	}

	virtual ~FileDisk()
	{
		close();
	}

	void open(const char* path, FileOpenMode::Enum mode)
	{
#if DERELICT_PLATFORM_POSIX
		_file = fopen(path, (mode == FileOpenMode::READ) ? "rb" : "wb");
		DE_ASSERT(_file != NULL, "fopen: errno = %d, path = '%s'", errno, path);
#endif
	}

	void close()
	{
		if (is_open())
		{
#if DERELICT_PLATFORM_POSIX
			fclose(_file);
			_file = NULL;
#endif
		}
	}

	bool is_open() const
	{
#if DERELICT_PLATFORM_POSIX
		return _file != NULL;
#endif
	}

	u32 size()
	{
#if DERELICT_PLATFORM_POSIX
		long pos = ftell(_file);
		DE_ASSERT(pos != -1, "ftell: errno = %d", errno);
		int err = fseek(_file, 0, SEEK_END);
		DE_ASSERT(err == 0, "fseek: errno = %d", errno);
		long size = ftell(_file);
		DE_ASSERT(size != -1, "ftell: errno = %d", errno);
		err = fseek(_file, (long)pos, SEEK_SET);
		DE_ASSERT(err == 0, "fseek: errno = %d", errno);
		DE_UNUSED(err);
		return (u32)size;
#endif
	}

	u32 position()
	{
#if DERELICT_PLATFORM_POSIX
		long pos = ftell(_file);
		DE_ASSERT(pos != -1, "ftell: errno = %d", errno);
		return (u32)pos;
#endif
	}

	bool end_of_file()
	{
#if DERELICT_PLATFORM_POSIX
		return feof(_file) != 0;
#endif
	}

	void seek(u32 position)
	{
#if DERELICT_PLATFORM_POSIX
		int err = fseek(_file, (long)position, SEEK_SET);
		DE_ASSERT(err == 0, "fseek: errno = %d", errno);
#endif
		DE_UNUSED(err);
	}

	void seek_to_end()
	{
#if DERELICT_PLATFORM_POSIX
		int err = fseek(_file, 0, SEEK_END);
		DE_ASSERT(err == 0, "fseek: errno = %d", errno);
#endif
		DE_UNUSED(err);
	}

	void skip(u32 bytes)
	{
#if DERELICT_PLATFORM_POSIX
		int err = fseek(_file, bytes, SEEK_CUR);
		DE_ASSERT(err == 0, "fseek: errno = %d", errno);
#endif
		DE_UNUSED(err);
	}

	u32 read(void* data, u32 size)
	{
		DE_ASSERT(data != NULL, "Data must be != NULL");
#if DERELICT_PLATFORM_POSIX
		size_t bytes_read = fread(data, 1, size, _file);
		DE_ASSERT(ferror(_file) == 0, "fread error");
		return (u32)bytes_read;
#endif
	}

	u32 write(const void* data, u32 size)
	{
		DE_ASSERT(data != NULL, "Data must be != NULL");
#if DERELICT_PLATFORM_POSIX
		size_t bytes_written = fwrite(data, 1, size, _file);
		DE_ASSERT(ferror(_file) == 0, "fwrite error");
		return (u32)bytes_written;
#endif
	}

	void flush()
	{
#if DERELICT_PLATFORM_POSIX
		int err = fflush(_file);
		DE_ASSERT(err == 0, "fflush: errno = %d", errno);
#endif
		DE_UNUSED(err);
	}
};

FilesystemDisk::FilesystemDisk(Allocator& a)
	: _allocator(&a)
	, _prefix(a)
{
}

void FilesystemDisk::set_prefix(const char* prefix)
{
	_prefix.set(prefix, strlen32(prefix));
}

File* FilesystemDisk::open(const char* path, FileOpenMode::Enum mode)
{
	DE_ENSURE(NULL != path);

    TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	FileDisk* file = DE_NEW(*_allocator, FileDisk)();
	file->open(abs_path.c_str(), mode);
	return file;
}

void FilesystemDisk::close(File& file)
{
	DE_DELETE(*_allocator, &file);
}

bool FilesystemDisk::exists(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	return os::exists(abs_path.c_str());
}

bool FilesystemDisk::is_directory(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	return os::is_directory(abs_path.c_str());
}

bool FilesystemDisk::is_file(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	return os::is_file(abs_path.c_str());
}

u64 FilesystemDisk::last_modified_time(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	return os::mtime(abs_path.c_str());
}

void FilesystemDisk::create_directory(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	if (!os::exists(abs_path.c_str()))
		os::create_directory(abs_path.c_str());
}

void FilesystemDisk::delete_directory(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	os::delete_directory(abs_path.c_str());
}

void FilesystemDisk::create_file(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	os::create_file(abs_path.c_str());
}

void FilesystemDisk::delete_file(const char* path)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	os::delete_file(abs_path.c_str());
}

void FilesystemDisk::list_files(const char* path, Vector<DynamicString>& files)
{
	DE_ENSURE(NULL != path);

	TempAllocator256 ta;
	DynamicString abs_path(ta);
	get_absolute_path(path, abs_path);

	os::list_files(abs_path.c_str(), files);
}

void FilesystemDisk::get_absolute_path(const char* path, DynamicString& os_path)
{
	if (path::is_absolute(path))
	{
		os_path = path;
		return;
	}

	path::join(_prefix.c_str(), path, os_path);
}

} // namespace derelict
