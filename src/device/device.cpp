#include "array.h"
#include "config.h"
#include "config_resource.h"
#include "console_api.h"
#include "console_server.h"
#include "data_compiler.h"
#include "device.h"
#include "device_event_queue.h"
#include "file.h"
#include "filesystem.h"
#include "filesystem_disk.h"
#include "input_device.h"
#include "input_manager.h"
#include "json_object.h"
#include "log.h"
#include "lua_environment.h"
#include "lua_resource.h"
#include "map.h"
#include "matrix4x4.h"
#include "memory.h"
#include "os.h"
#include "package_resource.h"
#include "path.h"
#include "profiler.h"
#include "proxy_allocator.h"
#include "resource_loader.h"
#include "resource_manager.h"
#include "resource_package.h"
#include "sjson.h"
#include "string_stream.h"
#include "string_utils.h"
#include "temp_allocator.h"
#include "types.h"
#include "vector3.h"
#include <bgfx/bgfx.h>
#include <bx/allocator.h>
#define MAX_SUBSYSTEMS_HEAP 8 * 1024 * 1024
namespace derelict
{
extern bool next_event(OsEvent& ev);

struct BgfxCallback : public bgfx::CallbackI
{
	virtual void fatal(bgfx::Fatal::Enum _code, const char* _str)
	{
		DE_ASSERT(false, "Fatal error: 0x%08x: %s", _code, _str);
		DE_UNUSED(_code);
		DE_UNUSED(_str);
	}

	virtual void traceVargs(const char* /*_filePath*/, u16 /*_line*/, const char* _format, va_list _argList)
	{
		char buf[2048];
		strncpy(buf, _format, sizeof(buf));
		buf[strlen32(buf)-1] = '\0'; // Remove trailing newline
		logdv(buf, _argList);
	}

	virtual u32 cacheReadSize(u64 /*_id*/)
	{
		return 0;
	}

	virtual bool cacheRead(u64 /*_id*/, void* /*_data*/, u32 /*_size*/)
	{
		return false;
	}

	virtual void cacheWrite(u64 /*_id*/, const void* /*_data*/, u32 /*_size*/)
	{
	}

	virtual void screenShot(const char* /*_filePath*/, u32 /*_width*/, u32 /*_height*/, u32 /*_pitch*/, const void* /*_data*/, u32 /*_size*/, bool /*_yflip*/)
	{
	}

	virtual void captureBegin(u32 /*_width*/, u32 /*_height*/, u32 /*_pitch*/, bgfx::TextureFormat::Enum /*_format*/, bool /*_yflip*/)
	{
	}

	virtual void captureEnd()
	{
	}

	virtual void captureFrame(const void* /*_data*/, u32 /*_size*/)
	{
	}
};

struct BgfxAllocator : public bx::AllocatorI
{
	BgfxAllocator(Allocator& a)
		: _allocator(a, "bgfx")
	{
	}

	virtual void* realloc(void* _ptr, size_t _size, size_t _align, const char* /*_file*/, u32 /*_line*/)
	{
		if (!_ptr)
			return _allocator.allocate((u32)_size, (u32)_align == 0 ? 1 : (u32)_align);

		if (_size == 0)
		{
			_allocator.deallocate(_ptr);
			return NULL;
		}

		// Realloc
		void* p = _allocator.allocate((u32)_size, (u32)_align == 0 ? 1 : (u32)_align);
		_allocator.deallocate(_ptr);
		return p;
	}

private:

	ProxyAllocator _allocator;
};
    
Device::Device(const DeviceOptions& opts)
    : _allocator(default_allocator(), MAX_SUBSYSTEMS_HEAP) 
    , _device_options(opts)
    , _boot_config(default_allocator())
    , _console_server(NULL)
    , _data_compiler(NULL)
    , _bundle_filesystem(NULL)
    , _last_log(NULL)
    , _resource_loader(NULL)
    , _resource_manager(NULL)
    , _bgfx_allocator(NULL)
	, _bgfx_callback(NULL)
    , _input_manager(NULL)
    , _lua_environment(NULL)
    , _display(NULL)
    , _window(NULL)
    , _width(0)
    , _height(0)
    , _quit(false)
    , _paused(false)
    , _frame_count(0)
    , _last_delta_time(0.0f)
    , _time_since_start(0.0)
{
}
bool Device::process_events(s16& mouse_x, s16& mouse_y, s16& mouse_last_x, s16& mouse_last_y, bool vsync)
{
    InputManager* im = _input_manager;
    bool exit = false;
	bool reset = false;
	OsEvent event;
	while(next_event(event))
	{
		if (event.type == OsEventType::NONE)
			continue;
        
        switch(event.type)
        {
        case OsEventType::BUTTON:
            {
                const ButtonEvent ev  = event.button;
                switch(ev.device_id)
                {
                case InputDeviceType::KEYBOARD:
                    im->keyboard()->set_button_state(ev.button_num, ev.pressed);
                    break;
                }
            }
            break;
        case OsEventType::EXIT:
            exit = true;
            break;
        }
    }
    return exit;
}
void Device::run()
{
    _console_server = DE_NEW(_allocator, ConsoleServer)(default_allocator());
	load_console_api(*_console_server);
    
    namespace cor = config_resource_internal;
    namespace lur = lua_resource_internal;
    namespace pkr = package_resource_internal;
  	
  	bool do_continue = true;

#if DERELICT_PLATFORM_OSX
    if (_device_options._do_compile || _device_options._server)
    {
        _data_compiler = DE_NEW(_allocator, DataCompiler)();
        _data_compiler->register_compiler(RESOURCE_TYPE_CONFIG,           RESOURCE_VERSION_CONFIG,           cor::compile);
        _data_compiler->register_compiler(RESOURCE_TYPE_PACKAGE,          RESOURCE_VERSION_PACKAGE,          pkr::compile);
        _data_compiler->register_compiler(RESOURCE_TYPE_SCRIPT,           RESOURCE_VERSION_SCRIPT,           lur::compile);
        
        _data_compiler->map_source_dir("", _device_options._source_dir);
        
        if (_device_options._map_source_dir_name)
		{
			_data_compiler->map_source_dir(_device_options._map_source_dir_name
				, _device_options._map_source_dir_prefix
				);
		}
		
		_data_compiler->scan();
		
		if (_device_options._server)
		{
			_console_server->listen(DERELICT_DEFAULT_COMPILER_PORT, false);
			
			while (true)
			{
				_console_server->update();
				os::sleep(60);
			}
		}
		else
		{
			const char* data_dir = _device_options._data_dir;
			const char* platform = _device_options._platform;
			do_continue = _data_compiler->compile(data_dir, platform);
			do_continue = do_continue && _device_options._do_continue;
		}
    }
#endif // DERELICT_PLATFORM_OSX

    if (do_continue)
    {
        _console_server->listen(_device_options._console_port, _device_options._wait_console);
        
        const char* data_dir = _device_options._data_dir;
		if (!data_dir)
		{
			char buf[1024];
			data_dir = os::getcwd(buf, sizeof(buf));
		}
		_bundle_filesystem = DE_NEW(_allocator, FilesystemDisk)(default_allocator());
		((FilesystemDisk*)_bundle_filesystem)->set_prefix(data_dir);
		if (!_bundle_filesystem->exists(data_dir))
			_bundle_filesystem->create_directory(data_dir);
		
		_last_log = _bundle_filesystem->open(DERELICT_LAST_LOG, FileOpenMode::WRITE);
        
        logi("Initializing derelict engine %s...", version());
        
        profiler_globals::init();
        
        _resource_loader  = DE_NEW(_allocator, ResourceLoader)(*_bundle_filesystem);
		_resource_manager = DE_NEW(_allocator, ResourceManager)(*_resource_loader);
        _resource_manager->register_type(RESOURCE_TYPE_SCRIPT,           lur::load, lur::unload, NULL,        NULL        );
        _resource_manager->register_type(RESOURCE_TYPE_PACKAGE,          pkr::load, pkr::unload, NULL,        NULL        );
        _resource_manager->register_type(RESOURCE_TYPE_CONFIG,           cor::load, cor::unload, NULL,        NULL        );
        
        // Read config
        {
            TempAllocator512 ta;
			DynamicString boot_dir(ta);
            if (_device_options._boot_dir != NULL)
			{
				boot_dir += _device_options._boot_dir;
				boot_dir += '/';
			}
			boot_dir += DERELICT_BOOT_CONFIG;
            const StringId64 config_name(boot_dir.c_str());
			_resource_manager->load(RESOURCE_TYPE_CONFIG, config_name);
			_resource_manager->flush();
			_boot_config.parse((const char*)_resource_manager->get(RESOURCE_TYPE_CONFIG, config_name));
			_resource_manager->unload(RESOURCE_TYPE_CONFIG, config_name);
        }

        // Init all remaining subsystems
        _bgfx_allocator = DE_NEW(_allocator, BgfxAllocator)(default_allocator());
		_bgfx_callback  = DE_NEW(_allocator, BgfxCallback)();
        
        _display = display::create(_allocator);
        _window = window::create(_allocator);
        _window->open(_device_options._window_x
            , _device_options._window_y
            , _boot_config.window_w
            , _boot_config.window_h
            , _device_options._parent_window
            );
        _window->set_title(_boot_config.window_title.c_str());
		_window->set_fullscreen(_boot_config.fullscreen);
		_window->bgfx_setup();

		bgfx::init(bgfx::RendererType::Count
			, BGFX_PCI_ID_NONE
			, 0
			, _bgfx_callback
			, _bgfx_allocator
			);
       
        _input_manager    = DE_NEW(_allocator, InputManager)(default_allocator());
        _lua_environment  = DE_NEW(_allocator, LuaEnvironment)();
        
        ResourcePackage* boot_package = create_resource_package(_boot_config.boot_package_name);
		boot_package->load();
		boot_package->flush();
        
        _lua_environment->load_libs();
		_lua_environment->execute((LuaResource*)_resource_manager->get(RESOURCE_TYPE_SCRIPT, _boot_config.boot_script_name));
		_lua_environment->call_global("init", 0);
        
        logd("Engine initialized");
        
        s16 mouse_x = 0;
		s16 mouse_y = 0;
		s16 mouse_last_x = 0;
		s16 mouse_last_y = 0;
        
        s64 last_time = os::clocktime();
		s64 curr_time;
        
        while(!process_events(mouse_x, mouse_y, mouse_last_x, mouse_last_y, _boot_config.vsync) && !_quit)
        {
            curr_time = os::clocktime();
			const s64 time = curr_time - last_time;
			last_time = curr_time;
			const f64 freq = (f64)os::clockfrequency();
			_last_delta_time = f32(time * (1.0 / freq));
			_time_since_start += _last_delta_time;
			profiler_globals::clear();
			_console_server->update();
			RECORD_FLOAT("device.dt", _last_delta_time);
			RECORD_FLOAT("device.fps", 1.0f/_last_delta_time);
            if (!_paused)
			{
				_resource_manager->complete_requests();
				{
					const s64 t0 = os::clocktime();
					_lua_environment->call_global("update", 1, ARGUMENT_FLOAT, last_delta_time());
					const s64 t1 = os::clocktime();
					RECORD_FLOAT("lua.update", f32((t1 - t0)*(1.0 / freq)));
				}
				{
					const s64 t0 = os::clocktime();
					_lua_environment->call_global("render", 1, ARGUMENT_FLOAT, last_delta_time());
					const s64 t1 = os::clocktime();
					RECORD_FLOAT("lua.render", f32((t1 - t0)*(1.0 / freq)));
				}
			}
			_input_manager->update();

			const bgfx::Stats* stats = bgfx::getStats();
			RECORD_FLOAT("bgfx.gpu_time", f32(f64(stats->gpuTimeEnd - stats->gpuTimeBegin)*1000.0/stats->gpuTimerFreq));
			RECORD_FLOAT("bgfx.cpu_time", f32(f64(stats->cpuTimeEnd - stats->cpuTimeBegin)*1000.0/stats->cpuTimerFreq));

			bgfx::frame();
            profiler_globals::flush();
			
			_lua_environment->reset_temporaries();
			
			_frame_count++;
        }

        logi("Engine shutting down...");

         _lua_environment->call_global("shutdown", 0);
        
        boot_package->unload();
		destroy_resource_package(*boot_package);
        
        DE_DELETE(_allocator, _lua_environment);
        DE_DELETE(_allocator, _input_manager);
        DE_DELETE(_allocator, _resource_manager);
		DE_DELETE(_allocator, _resource_loader);
        
        bgfx::shutdown();
        _window->close();
        window::destroy(_allocator, *_window);
        display::destroy(_allocator, *_display);
        DE_DELETE(_allocator, _bgfx_callback);
		DE_DELETE(_allocator, _bgfx_allocator);

        if (_last_log)
			_bundle_filesystem->close(*_last_log);

        DE_DELETE(_allocator, _bundle_filesystem);

        profiler_globals::shutdown();
    }
	DE_DELETE(_allocator, _data_compiler);
    _console_server->shutdown();
    DE_DELETE(_allocator, _console_server);
    
	_allocator.clear();
}
void Device::quit()
{
	_quit = true;
}
void Device::pause()
{
	_paused = true;
	logi("Engine paused.");
}
void Device::unpause()
{
	_paused = false;
	logi("Engine unpaused.");
}
u64 Device::frame_count() const
{
	return _frame_count;
}
f32 Device::last_delta_time() const
{
	return _last_delta_time;
}
f64 Device::time_since_start() const
{
	return _time_since_start;
}
ResourcePackage* Device::create_resource_package(StringId64 id)
{
	return DE_NEW(default_allocator(), ResourcePackage)(id, *_resource_manager);
}
void Device::destroy_resource_package(ResourcePackage& rp)
{
	DE_DELETE(default_allocator(), &rp);
}
void Device::reload(StringId64 type, StringId64 name)
{
	_resource_manager->reload(type, name);
	const void* new_resource = _resource_manager->get(type, name);
	if (type == RESOURCE_TYPE_SCRIPT)
	{
		_lua_environment->execute((const LuaResource*)new_resource);
	}
}
static StringStream& sanitize(StringStream& ss, const char* msg)
{
	using namespace string_stream;
	const char* ch = msg;
	for (; *ch; ch++)
	{
		if (*ch == '"' || *ch == '\\')
			ss << "\\";
		ss << *ch;
	}
	return ss;
}
static const char* s_severity_map[] = { "info", "warning", "error", "debug" };
DE_STATIC_ASSERT(countof(s_severity_map) == LogSeverity::COUNT);
void Device::log(const char* msg, LogSeverity::Enum severity)
{
	if (_last_log)
	{
		_last_log->write(msg, strlen32(msg));
		_last_log->write("\n", 1);
		_last_log->flush();
	}
	if (_console_server)
	{
		TempAllocator4096 ta;
		StringStream json(ta);
		json << "{\"type\":\"message\",";
		json << "\"severity\":\"" << s_severity_map[severity] << "\",";
		json << "\"message\":\""; sanitize(json, msg) << "\"}";
		_console_server->send(string_stream::c_str(json));
	}
}
char _buffer[sizeof(Device)];
Device* _device = NULL;
void run(const DeviceOptions& opts)
{
	DE_ASSERT(_device == NULL, "derelict already initialized");
	_device = new (_buffer) Device(opts);
	_device->run();
	_device->~Device();
	_device = NULL;
}
Device* device()
{
	return derelict::_device;
}
} // namespace derelict