#pragma once

/// @defgroup Filesystem Filesystem
/// @ingroup Core
namespace derelict
{
class Filesystem;
class File;

/// Enumerates file open modes.
///
/// @ingroup Filesystem
struct FileOpenMode
{
	enum Enum
	{
		READ,
		WRITE
	};
};

} // namespace derelict
