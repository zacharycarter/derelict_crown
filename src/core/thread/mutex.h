#pragma once

#include "error.h"
#include "types.h"
#include "platform.h"

#if DERELICT_PLATFORM_POSIX
	#include <pthread.h>
#endif

namespace derelict
{
/// Mutex.
///
/// @ingroup Thread
struct Mutex
{
#if DERELICT_PLATFORM_POSIX
	pthread_mutex_t _mutex;
#elif DERELICT_PLATFORM_WINDOWS
	CRITICAL_SECTION _cs;
#endif

	Mutex()
	{
#if DERELICT_PLATFORM_POSIX
		pthread_mutexattr_t attr;
		int err = pthread_mutexattr_init(&attr);
		DE_ASSERT(err == 0, "pthread_mutexattr_init: errno = %d", err);
		err = pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
		DE_ASSERT(err == 0, "pthread_mutexattr_settype: errno = %d", err);
		err = pthread_mutex_init(&_mutex, &attr);
		DE_ASSERT(err == 0, "pthread_mutex_init: errno = %d", err);
		err = pthread_mutexattr_destroy(&attr);
		DE_ASSERT(err == 0, "pthread_mutexattr_destroy: errno = %d", err);
		DE_UNUSED(err);
#endif
	}

	~Mutex()
	{
#if DERELICT_PLATFORM_POSIX
		int err = pthread_mutex_destroy(&_mutex);
		DE_ASSERT(err == 0, "pthread_mutex_destroy: errno = %d", err);
		DE_UNUSED(err);
#endif
	}

	/// Locks the mutex.
	void lock()
	{
#if DERELICT_PLATFORM_POSIX
		int err = pthread_mutex_lock(&_mutex);
		DE_ASSERT(err == 0, "pthread_mutex_lock: errno = %d", err);
		DE_UNUSED(err);
#endif
	}

	/// Unlocks the mutex.
	void unlock()
	{
#if DERELICT_PLATFORM_POSIX
		int err = pthread_mutex_unlock(&_mutex);
		DE_ASSERT(err == 0, "pthread_mutex_unlock: errno = %d", err);
		DE_UNUSED(err);
#endif
	}

private:

	// Disable copying.
	Mutex(const Mutex&);
	Mutex& operator=(const Mutex&);
};

/// Automatically locks a mutex when created and unlocks when destroyed.
///
/// @ingroup Thread
struct ScopedMutex
{
	Mutex& _mutex;

	/// Locks the mutex @a m.
	ScopedMutex(Mutex& m)
		: _mutex(m)
	{
		_mutex.lock();
	}

	/// Unlocks the mutex passed to ScopedMutex::ScopedMutex()
	~ScopedMutex()
	{
		_mutex.unlock();
	}

private:

	// Disable copying
	ScopedMutex(const ScopedMutex&);
	ScopedMutex& operator=(const ScopedMutex&);
};

} // namespace derelict
