#pragma once

#include "container_types.h"

/// @defgroup String String
/// @ingroup Core
namespace derelict
{
struct DynamicString;
struct FixedString;
struct StringId32;
struct StringId64;

typedef StringId64 ResourceId;

/// Stream of characters.
///
/// @ingroup String
typedef Array<char> StringStream;
} // namespace derelict
