#pragma once

#include "config.h"

/// @defgroup Error Error
/// @ingroup Core
namespace derelict
{
/// Error management.
///
/// @ingroup Error
namespace error
{
    /// Aborts the program execution logging an error message and the stacktrace if
	/// the platform supports it.
	void abort(const char* file, int line, const char* format, ...);

    /// Prints the current call stack.
	void print_callstack();
} // namespace error

} // namespace derelict

#if DERELICT_DEBUG
    	#define DE_ASSERT(condition, msg, ...)              \
		do                                                  \
		{                                                   \
			if (!(condition))                               \
			{                                               \
				derelict::error::abort(__FILE__             \
					, __LINE__                              \
					, "\nAssertion failed: %s\n\t" msg "\n" \
					, #condition                            \
					, ##__VA_ARGS__                         \
					);                                      \
			}                                               \
		} while (0)
#else
    #define DE_ASSERT(...) ((void)0)
#endif // DERELICT_DEBUG

#define DE_FATAL(msg, ...) DE_ASSERT(false, msg, ##__VA_ARGS__)
#define DE_ENSURE(condition) DE_ASSERT(condition, "")
