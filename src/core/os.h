#pragma once

#include "error.h"
#include "string_types.h"
#include "types.h"
#include "platform.h"

#if DERELICT_PLATFORM_POSIX
	#include <dlfcn.h>    // dlopen, dlclose, dlsym
	#include <errno.h>
	#include <stdio.h>    // fputs
	#include <string.h>   // memset
	#include <sys/stat.h> // lstat, mknod, mkdir
	#include <sys/wait.h> // wait
	#include <time.h>     // clock_gettime
	#include <unistd.h>   // access, unlink, rmdir, getcwd, fork, execv
 	#include <stdlib.h>   // getenv
#endif

#if DERELICT_PLATFORM_OSX
    #include <sys/time.h>
#endif

/// @defgroup OS OS
/// @ingroup Core
namespace derelict
{
/// Operating system functions.
///
/// @ingroup OS
namespace os
{
    inline s64 clocktime()
	{
#if DERELICT_PLATFORM_OSX
		struct timeval now;
		gettimeofday(&now, NULL);
		return now.tv_sec * s64(1000000) + now.tv_usec;
#endif
	}

	inline s64 clockfrequency()
	{
#if DERELICT_PLATFORM_OSX
		return s64(1000000);
#endif
	}

    /// Suspends execution for @a ms milliseconds.
	inline void sleep(u32 ms)
	{
#if DERELICT_PLATFORM_OSX
		usleep(ms * 1000);
#endif
	}

    /// Logs the message @a msg.
	inline void log(const char* msg)
	{
		fputs(msg, stdout);
		fflush(stdout);
	}

    /// Returns whether the @a path exists.
	inline bool exists(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		return access(path, F_OK) != -1;
#endif
	}

    /// Returns whether @a path is a directory.
	inline bool is_directory(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		struct stat info;
		memset(&info, 0, sizeof(info));
		int err = lstat(path, &info);
		DE_ASSERT(err == 0, "lstat: errno = %d", errno);
		DE_UNUSED(err);
		return ((S_ISDIR(info.st_mode) == 1) && (S_ISLNK(info.st_mode) == 0));
#endif
	}

    /// Returns whether @a path is a regular file.
	inline bool is_file(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		struct stat info;
		memset(&info, 0, sizeof(info));
		int err = lstat(path, &info);
		DE_ASSERT(err == 0, "lstat: errno = %d", errno);
		DE_UNUSED(err);
		return ((S_ISREG(info.st_mode) == 1) && (S_ISLNK(info.st_mode) == 0));
#endif
	}

    	/// Returns the last modification time of @a path.
	inline u64 mtime(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		struct stat info;
		memset(&info, 0, sizeof(info));
		int err = lstat(path, &info);
		DE_ASSERT(err == 0, "lstat: errno = %d", errno);
		DE_UNUSED(err);
		return info.st_mtime;
#endif
	}

    /// Creates a regular file named @a path.
	inline void create_file(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		int err = ::mknod(path, 0644 | S_IFREG , 0);
		DE_ASSERT(err == 0, "mknod: errno = %d", errno);
		DE_UNUSED(err);
#endif
	}

	/// Deletes the file at @a path.
	inline void delete_file(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		int err = ::unlink(path);
		DE_ASSERT(err == 0, "unlink: errno = %d", errno);
		DE_UNUSED(err);
#endif
	}

	/// Creates a directory named @a path.
	inline void create_directory(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		int err = ::mkdir(path, 0755);
		DE_ASSERT(err == 0, "mkdir: errno = %d", errno);
		DE_UNUSED(err);
#endif
	}

	/// Deletes the directory at @a path.
	inline void delete_directory(const char* path)
	{
#if DERELICT_PLATFORM_OSX
		int err = ::rmdir(path);
		DE_ASSERT(err == 0, "rmdir: errno = %d", errno);
		DE_UNUSED(err);
#endif
	}

    /// Returns the list of @a files at the given @a path.
	void list_files(const char* path, Vector<DynamicString>& files);

	/// Returns the current working directory.
	inline const char* getcwd(char* buf, u32 size)
	{
#if DERELICT_PLATFORM_OSX
		return ::getcwd(buf, size);
#endif
	}

	/// Returns the value of the environment variable @a name.
	inline const char* getenv(const char* name)
	{
#if DERELICT_PLATFORM_OSX
		return ::getenv(name);
#endif
	}

	/// Executes the process @a path with the given @a args and returns its exit code.
	/// It fills @a output with stdout and stderr.
	int execute_process(const char* path, const char* args, StringStream& output);

} // namespace os
} // namespace derelict
