#pragma once

#include <stdint.h>

/// @defgroup Core Core

namespace derelict
{
/// @addtogroup Core
/// @{
typedef int8_t   s8;
typedef uint8_t  u8;
typedef int16_t  s16;
typedef uint16_t u16;
typedef int32_t  s32;
typedef uint32_t u32;
typedef int64_t  s64;
typedef uint64_t u64;
typedef float    f32;
typedef double   f64;
/// @}

} // namespace derelict

#if !defined(alignof)
	#define alignof(x) __alignof(x)
#endif

#if !defined(__va_copy)
	#define __va_copy(dest, src) (dest = src)
#endif

#ifndef NULL
	#define NULL 0
#endif

#define countof(arr) (sizeof(arr)/sizeof(arr[0]))

#define DE_UNUSED(x) do { (void)(x); } while (0)
#define DE_CONCATENATE1(a, b) a ## b
#define DE_CONCATENATE(a, b) DE_CONCATENATE1(a, b)
#define DE_STATIC_ASSERT(condition) typedef int DE_CONCATENATE(STATIC_ASSERT,__LINE__)[condition ? 1 : -1]

#if defined(__GNUC__) || defined(__clang__)
	#define DE_THREAD __thread
#else
	#error "Compiler not supported"
#endif
