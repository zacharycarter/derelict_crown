# derelict
derelict is a science-fiction roguelike game being written in C++ and Lua.

## platforms
derelict aims to run and compile on a wide variety of platforms, at the moment derelict has support for:  
  

| compile | runtime |
|---------|---------|
| osx     | osx     |

## cloning
```
git clone --recursive git@gitlab.com:zacharycarter/derelict.git
```

## building
```
$ make derelict-${configuration}
```
${configuration} is `<platform>-[arch-]<debug/release>[32|64]`. E.g.
```
$ make derelict-osx-debug64
```

## technical
bgfx  
luajit -- wip

## credits
[taylor001](https://github.com/taylor001/) and his [pepper/crown engine](https://github.com/taylor001/crown) - Much of the core of derelict was borrowed from the pepper / crown engine.
