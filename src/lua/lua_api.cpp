#include "color4.h"
#include "console_server.h"
#include "device.h"
#include "dynamic_string.h"
#include "guid.h"
#include "input_device.h"
#include "input_manager.h"
#include "lua_environment.h"
#include "lua_stack.h"
#include "math_types.h"
#include "math_utils.h"
#include "matrix4x4.h"
#include "plane3.h"
#include "quaternion.h"
#include "resource_manager.h"
#include "string_stream.h"
#include "temp_allocator.h"
#include "vector2.h"
#include "vector3.h"

namespace derelict
{

static int input_device_name(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_string(dev.name());
	return 1;
}

static int input_device_connected(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_bool(dev.connected());
	return 1;
}

static int input_device_num_buttons(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_int(dev.num_buttons());
	return 1;
}

static int input_device_num_axes(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_int(dev.num_axes());
	return 1;
}

static int input_device_pressed(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_bool(dev.pressed(stack.get_int(1)));
	return 1;
}

static int input_device_released(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_bool(dev.released(stack.get_int(1)));
	return 1;
}

static int input_device_any_pressed(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_bool(dev.any_pressed());
	return 1;
}

static int input_device_any_released(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	stack.push_bool(dev.any_released());
	return 1;
}

static int input_device_button_name(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	const char* name = dev.button_name(stack.get_int(1));

	if (name != NULL)
		stack.push_string(name);
	else
		stack.push_nil();

	return 1;
}

static int input_device_button_id(lua_State* L, InputDevice& dev)
{
	LuaStack stack(L);
	const u8 id = dev.button_id(stack.get_string_id_32(1));

	if (id != UINT8_MAX)
		stack.push_int(id);
	else
		stack.push_nil();

	return 1;
}

#define KEYBOARD_FN(name) keyboard_##name

#define KEYBOARD(name) static int KEYBOARD_FN(name)(lua_State* L)\
	{ return input_device_##name(L, *device()->_input_manager->keyboard()); }

KEYBOARD(name)
KEYBOARD(connected)
KEYBOARD(num_buttons)
KEYBOARD(num_axes)
KEYBOARD(pressed)
KEYBOARD(released)
KEYBOARD(any_pressed)
KEYBOARD(any_released)
// KEYBOARD(axis)
KEYBOARD(button_name)
// KEYBOARD(axis_name)
KEYBOARD(button_id)
// KEYBOARD(axis_id)

static int device_quit(lua_State* /*L*/)
{
	device()->quit();
	return 0;
}

void load_api(LuaEnvironment& env)
{
	env.add_module_function("Keyboard", "name",         KEYBOARD_FN(name));
	env.add_module_function("Keyboard", "connected",    KEYBOARD_FN(connected));
	env.add_module_function("Keyboard", "num_buttons",  KEYBOARD_FN(num_buttons));
	env.add_module_function("Keyboard", "num_axes",     KEYBOARD_FN(num_axes));
	env.add_module_function("Keyboard", "pressed",      KEYBOARD_FN(pressed));
	env.add_module_function("Keyboard", "released",     KEYBOARD_FN(released));
	env.add_module_function("Keyboard", "any_pressed",  KEYBOARD_FN(any_pressed));
	env.add_module_function("Keyboard", "any_released", KEYBOARD_FN(any_released));
	env.add_module_function("Keyboard", "button_name",  KEYBOARD_FN(button_name));
	env.add_module_function("Keyboard", "button_id",    KEYBOARD_FN(button_id));

	env.add_module_function("Device", "quit",                     device_quit);
}

} // namespace derelict
