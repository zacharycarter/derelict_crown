#pragma once

#include "error.h"
#include "semaphore.h"
#include "types.h"
#include "platform.h"

#if DERELICT_PLATFORM_POSIX
    #include <pthread.h>
#endif

/// @defgroup Thread Thread
/// @ingroup Core
namespace derelict
{
/// Thread.
///
/// @ingroup Thread.
struct Thread
{
	typedef s32 (*ThreadFunction)(void* data);

	ThreadFunction _function;
	void* _user_data;
	Semaphore _sem;
	bool _is_running;
#if DERELICT_PLATFORM_POSIX
	pthread_t _handle;
#endif

	Thread()
		: _function(NULL)
		, _user_data(NULL)
		, _is_running(false)
#if DERELICT_PLATFORM_POSIX
		, _handle(0)
#endif
	{
	}

	~Thread()
	{
		if (_is_running)
			stop();
	}

	void start(ThreadFunction func, void* user_data = NULL, u32 stack_size = 0)
	{
		DE_ASSERT(!_is_running, "Thread is already running");
		DE_ASSERT(func != NULL, "Function must be != NULL");
		_function = func;
		_user_data = user_data;

#if DERELICT_PLATFORM_POSIX
		pthread_attr_t attr;
		int err = pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
		DE_ASSERT(err == 0, "pthread_attr_init: errno = %d", err);

		if (stack_size != 0)
		{
			err = pthread_attr_setstacksize(&attr, stack_size);
			DE_ASSERT(err == 0, "pthread_attr_setstacksize: errno = %d", err);
		}

		err = pthread_create(&_handle, &attr, thread_proc, this);
		DE_ASSERT(err == 0, "pthread_create: errno = %d", err);

		err = pthread_attr_destroy(&attr);
		DE_ASSERT(err == 0, "pthread_attr_destroy: errno = %d", err);
		DE_UNUSED(err);
#endif

		_is_running = true;
		_sem.wait();
	}

	void stop()
	{
		DE_ASSERT(_is_running, "Thread is not running");

#if DERELICT_PLATFORM_POSIX
		int err = pthread_join(_handle, NULL);
		DE_ASSERT(err == 0, "pthread_join: errno = %d", err);
		DE_UNUSED(err);
		_handle = 0;
#endif

		_is_running = false;
	}

	bool is_running()
	{
		return _is_running;
	}

private:

	s32 run()
	{
		_sem.post();
		return _function(_user_data);
	}

#if DERELICT_PLATFORM_POSIX
	static void* thread_proc(void* arg)
	{
		static s32 result = -1;
		result = ((Thread*)arg)->run();
		return (void*)&result;
	}
#endif

private:

	// Disable copying
	Thread(const Thread&);
	Thread& operator=(const Thread&);
};

} // namespace derelict
