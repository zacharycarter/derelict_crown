function derelict_project(_name, _kind, _defines)
    
    project("derelict" .. _name)
        kind(_kind)
        includedirs {
            DERELICT_DIR .. "src",
            DERELICT_DIR .. "src/core",
            DERELICT_DIR .. "src/core/containers",
            DERELICT_DIR .. "src/core/error",
            DERELICT_DIR .. "src/core/filesystem",
            DERELICT_DIR .. "src/core/json",
            DERELICT_DIR .. "src/core/math",
            DERELICT_DIR .. "src/core/memory",
            DERELICT_DIR .. "src/core/network",
            DERELICT_DIR .. "src/core/strings",
            DERELICT_DIR .. "src/core/thread",
            DERELICT_DIR .. "src/device",
            DERELICT_DIR .. "src/lua",
            DERELICT_DIR .. "src/resource",
            DERELICT_DIR .. "src/world",
            DERELICT_DIR .. "3rdparty/bgfx/include",
            DERELICT_DIR .. "3rdparty/bgfx/examples/common",
            DERELICT_DIR .. "3rdparty/bgfx/examples/common/entry",
            DERELICT_DIR .. "3rdparty/bx/include",
        }
        defines {
            _defines,
        }
        links {
            "bgfx",
            "bx",
            "lua",
        }
        if _OPTIONS["with-luajit"] then
            includedirs {
                DERELICT_DIR .. "3rdparty/LuaJIT/src",
            }
            configuration { "osx" }
                libdirs {
                    DERELICT_DIR .. "3rdparty/LuaJIT/pre/osx_x64",
                }
                links {
                    "luajit",
                }
                linkoptions {
                    "-pagezero_size 10000",
                    "-image_base 100000000",
                }
            configuration {}
        end

    configuration { "debug" }
        defines {
            "DERELICT_DEBUG=1"
        }

    files {
        DERELICT_DIR .. "src/**.h",
        DERELICT_DIR .. "src/**.cpp",  
    }

    configuration {}
    strip()
end