// Adapted from Branimir Karadžić's platform.h (https://github.com/bkaradzic/bx)
#pragma once
#define DERELICT_COMPILER_CLANG 0
#define DERELICT_COMPILER_GCC 0
#define DERELICT_PLATFORM_OSX 0
#define DERELICT_CPU_ARM  0
#define DERELICT_CPU_JIT  0
#define DERELICT_CPU_MIPS 0
#define DERELICT_CPU_PPC  0
#define DERELICT_CPU_X86  0
#define DERELICT_ARCH_32BIT 0
#define DERELICT_ARCH_64BIT 0
#define DERELICT_CPU_ENDIAN_BIG 0
#define DERELICT_CPU_ENDIAN_LITTLE 0
// http://sourceforge.net/apps/mediawiki/predef/index.php?title=Compilers
#if defined(__clang__)
	// clang defines __GNUC__
	#undef DERELICT_COMPILER_CLANG
	#define DERELICT_COMPILER_CLANG 1
#elif defined(__GNUC__)
	#undef DERELICT_COMPILER_GCC
	#define DERELICT_COMPILER_GCC 1
#else
	#error "DERELICT_COMPILER_* is not defined!"
#endif //
// http://sourceforge.net/apps/mediawiki/predef/index.php?title=Operating_Systems
#if defined(__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__)
#   undef DERELICT_PLATFORM_OSX
#   define DERELICT_PLATFORM_OSX 1
#else
#   error "DERELICT_PLATFORM_* is not defined!"
#endif //
#define DERELICT_PLATFORM_POSIX (DERELICT_PLATFORM_OSX)
// http://sourceforge.net/apps/mediawiki/predef/index.php?title=Architectures
#if defined(__arm__)
	#undef DERELICT_CPU_ARM
	#define DERELICT_CPU_ARM 1
	#define DERELICT_CACHE_LINE_SIZE 64
#elif defined(__MIPSEL__) || defined(__mips_isa_rev) // defined(mips)
	#undef DERELICT_CPU_MIPS
	#define DERELICT_CPU_MIPS 1
	#define DERELICT_CACHE_LINE_SIZE 64
#elif defined(_M_PPC) || defined(__powerpc__) || defined(__powerpc64__)
	#undef DERELICT_CPU_PPC
	#define DERELICT_CPU_PPC 1
	#define DERELICT_CACHE_LINE_SIZE 128
#elif defined(_M_IX86) || defined(_M_X64) || defined(__i386__) || defined(__x86_64__)
	#undef DERELICT_CPU_X86
	#define DERELICT_CPU_X86 1
	#define DERELICT_CACHE_LINE_SIZE 64
#else // PNaCl doesn't have CPU defined.
	#undef DERELICT_CPU_JIT
	#define DERELICT_CPU_JIT 1
	#define DERELICT_CACHE_LINE_SIZE 64
#endif //
#if defined(__x86_64__) || defined(_M_X64) || defined(__64BIT__) || defined(__powerpc64__) || defined(__ppc64__)
	#undef DERELICT_ARCH_64BIT
	#define DERELICT_ARCH_64BIT 64
#else
	#undef DERELICT_ARCH_32BIT
	#define DERELICT_ARCH_32BIT 32
#endif //
#if DERELICT_CPU_PPC
	#undef DERELICT_CPU_ENDIAN_BIG
	#define DERELICT_CPU_ENDIAN_BIG 1
#else
	#undef DERELICT_CPU_ENDIAN_LITTLE
	#define DERELICT_CPU_ENDIAN_LITTLE 1
#endif
#if DERELICT_COMPILER_GCC
	#define DERELICT_COMPILER_NAME "GCC"
#endif
#if DERELICT_PLATFORM_OSX
    #define DERELICT_PLATFORM_NAME "osx"
#endif // DERELICT_PLATFORM_
#if DERELICT_CPU_ARM
	#define DERELICT_CPU_NAME "ARM"
#elif DERELICT_CPU_MIPS
	#define DERELICT_CPU_NAME "MIPS"
#elif DERELICT_CPU_PPC
	#define DERELICT_CPU_NAME "PowerPC"
#elif DERELICT_CPU_JIT
	#define DERELICT_CPU_NAME "JIT-VM"
#elif DERELICT_CPU_X86
	#define DERELICT_CPU_NAME "x86"
#endif // DERELICT_CPU_
#if DERELICT_ARCH_32BIT
	#define DERELICT_ARCH_NAME "32-bit"
#elif DERELICT_ARCH_64BIT
	#define DERELICT_ARCH_NAME "64-bit"
#endif // DERELICT_ARCH_