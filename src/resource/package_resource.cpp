#include "array.h"
#include "compile_options.h"
#include "dynamic_string.h"
#include "file.h"
#include "filesystem.h"
#include "json_object.h"
#include "map.h"
#include "package_resource.h"
#include "reader_writer.h"
#include "sjson.h"
#include "string_id.h"
#include "temp_allocator.h"

namespace derelict
{
namespace package_resource_internal
{
	void compile_resources(const char* type, const JsonArray& names, Array<PackageResource::Resource>& output, CompileOptions& opts)
	{
		const StringId64 typeh = StringId64(type);

		for (u32 i = 0; i < array::size(names); ++i)
		{
			TempAllocator1024 ta;
			DynamicString name(ta);
			sjson::parse_string(names[i], name);

			DATA_COMPILER_ASSERT_RESOURCE_EXISTS(type, name.c_str(), opts);

			const StringId64 nameh = sjson::parse_resource_id(names[i]);
			array::push_back(output, PackageResource::Resource(typeh, nameh));
		}
	}

	void compile(const char* path, CompileOptions& opts)
	{
		Buffer buf = opts.read(path);

		TempAllocator4096 ta;
		JsonObject object(ta);
		sjson::parse(buf, object);

		JsonArray script(ta);

		if (json_object::has(object, "lua"))              sjson::parse_array(object["lua"], script);

		Array<PackageResource::Resource> resources(default_allocator());

		compile_resources("lua", script, resources, opts);

		// Write
		opts.write(RESOURCE_VERSION_PACKAGE);
		opts.write(array::size(resources));

		for (u32 i = 0; i < array::size(resources); ++i)
		{
			opts.write(resources[i].type);
			opts.write(resources[i].name);
		}
	}

	void* load(File& file, Allocator& a)
	{
		BinaryReader br(file);

		u32 version;
		br.read(version);
		DE_ASSERT(version == RESOURCE_VERSION_PACKAGE, "Wrong version");

		u32 num_resources;
		br.read(num_resources);

		PackageResource* pr = DE_NEW(a, PackageResource)(a);
		array::resize(pr->resources, num_resources);
		br.read(array::begin(pr->resources), sizeof(PackageResource::Resource)*num_resources);

		return pr;
	}

	void unload(Allocator& a, void* resource)
	{
		DE_DELETE(a, (PackageResource*)resource);
	}
} // namespace package_resource_internal

} // namespace derelict
