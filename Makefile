UNAME := $(shell uname)
ifeq ($(UNAME), $(filter $(UNAME), Linux))
	OS=linux
else ifeq ($(UNAME), $(filter $(UNAME), Darwin))
	OS=darwin
else
	OS=windows
endif
GENIE=3rdparty/bx/tools/bin/$(OS)/genie
derelict-osx-build:
	$(GENIE) --file=scripts/genie.lua --with-luajit --gcc=osx gmake
derelict-osx-debug64: derelict-osx-build
	make -R -C build/projects/gmake-osx config=debug64
derelict-osx: derelict-osx-debug64
.PHONY: clean
clean:
	@echo Cleaning...
	-@rm -rf build