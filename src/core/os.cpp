#include "dynamic_string.h"
#include "os.h"
#include "string_stream.h"
#include "temp_allocator.h"
#include "vector.h"
#include <string.h> // strcmp

#if DERELICT_PLATFORM_POSIX
    #include <dirent.h> // opendir, readdir
#endif

namespace derelict
{
namespace os
{
	void list_files(const char* path, Vector<DynamicString>& files)
	{
#if DERELICT_PLATFORM_POSIX
		DIR *dir;
		struct dirent *entry;

		if (!(dir = opendir(path)))
			return;

		while ((entry = readdir(dir)))
		{
			const char* dname = entry->d_name;

			if (!strcmp(dname, ".") || !strcmp(dname, ".."))
				continue;

			TempAllocator512 ta;
			DynamicString fname(ta);
			fname.set(dname, strlen32(dname));
			vector::push_back(files, fname);
		}

		closedir(dir);
#endif
	}

	int execute_process(const char* path, const char* args, StringStream& output)
	{
#if DERELICT_PLATFORM_POSIX
		TempAllocator512 ta;
		DynamicString cmd(ta);
		cmd += path;
		cmd += " 2>&1 ";
		cmd += args;
		FILE* file = popen(cmd.c_str(), "r");

		char buf[1024];
		while (fgets(buf, sizeof(buf), file) != NULL)
			output << buf;

		return pclose(file);
#endif
    }
} // namespace os

} // namespace derelict
