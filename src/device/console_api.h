# include "console_server.h"

namespace derelict
{
/// Loads console API into console server @a cs.
void load_console_api(ConsoleServer& cs);
} // namespace derelict
